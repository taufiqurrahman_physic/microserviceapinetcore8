﻿using SkillManagement.Entity;

namespace SkillManajement.Repository.Interface.ReadRepository
{
    public interface ISkillEmployeeQuery
    {
        Task<IEnumerable<SkillEmployee>> GetSkillEmployees();
        Task<SkillEmployee> GetSkillEmploye(string id);
    }
}
