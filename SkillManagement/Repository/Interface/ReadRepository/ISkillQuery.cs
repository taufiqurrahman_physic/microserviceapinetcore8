﻿using SkillManagement.Entity;

namespace SkillManajement.Repository.Interface.ReadRepository
{
    public interface ISkillQuery
    {
        Task<IEnumerable<Skill>> GetSkills();
        Task<Skill> GetSkill(string id);
    }
}
