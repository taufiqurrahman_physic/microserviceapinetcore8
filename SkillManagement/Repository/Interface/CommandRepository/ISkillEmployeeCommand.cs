﻿using SkillManagement.Entity;

namespace SkillManajement.Repository.Interface.CommandRepository
{
    public interface ISkillEmployeeCommand
    {
        Task<SkillEmployee> Create(SkillEmployee data);
        Task<SkillEmployee> Update(SkillEmployee data);
        Task<SkillEmployee> Delete(string id);
        Task<bool> DeleteByEmployeeId(string employeeId);
    }
}
