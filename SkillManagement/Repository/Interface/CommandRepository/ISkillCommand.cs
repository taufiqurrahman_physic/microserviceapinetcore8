﻿using SkillManagement.Entity;

namespace SkillManajement.Repository.Interface.CommandRepository
{
    public interface ISkillCommand
    {
        Task<Skill> Create(Skill data);
        Task<Skill> Update(Skill data);
        Task<Skill> Delete(string id);
        Task DeleteSkillEmployeeBySkillId(string skillId);
    }
}
