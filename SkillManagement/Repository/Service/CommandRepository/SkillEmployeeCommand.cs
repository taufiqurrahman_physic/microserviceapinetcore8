﻿using SkillManajement.Context;
using SkillManajement.Repository.Interface.CommandRepository;
using Microsoft.EntityFrameworkCore;
using SkillManagement.Entity;

namespace SkillManajement.Repository.Service.CommandRepository
{
    public class SkillEmployeeCommand : ISkillEmployeeCommand
    {
        private readonly AppDbContext _context;

        public SkillEmployeeCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<SkillEmployee> Create(SkillEmployee data)
        {
            if (data == null)
                return data;

            data.Id = Guid.NewGuid().ToString();

            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }

        public async Task<SkillEmployee> Delete(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return null;

            var existingData = await _context.SkillEmployees.FirstOrDefaultAsync(s=>s.Id == id);

            if(existingData == null) return existingData;

            _context.Remove(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task<bool> DeleteByEmployeeId(string employeeId)
        {
            if (string.IsNullOrWhiteSpace(employeeId)) return false;

            var existingDatas = _context.SkillEmployees.Where(s => s.EmployeeId == employeeId);

            if (existingDatas == null) return false;

            _context.RemoveRange(existingDatas);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<SkillEmployee> Update(SkillEmployee data)
        {
            if (data == null) return data;

            var existingData = await _context.SkillEmployees.FirstOrDefaultAsync(s => s.Id == data.Id);

            if(existingData == null) return existingData;

            existingData.SkillId = data.SkillId;
            existingData.EmployeeId = data.EmployeeId;

            _context.Update(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }
    }
}
