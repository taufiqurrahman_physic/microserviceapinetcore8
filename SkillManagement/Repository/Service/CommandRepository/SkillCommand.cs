﻿using SkillManajement.Context;
using SkillManajement.Repository.Interface.CommandRepository;
using Microsoft.EntityFrameworkCore;
using SkillManagement.Entity;

namespace SkillManajement.Repository.Service.CommandRepository
{
    public class SkillCommand : ISkillCommand
    {
        private readonly AppDbContext _context;

        public SkillCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Skill> Create(Skill data)
        {
            if (data == null)
                return data;

            data.Id = Guid.NewGuid().ToString();

            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }

        public async Task<Skill> Delete(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return null;

            var existingData = await _context.Skills.FirstOrDefaultAsync(s=>s.Id == id);

            if(existingData == null) return existingData;

            _context.Remove(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task<Skill> Update(Skill data)
        {
            if (data == null) return data;

            var existingData = await _context.Skills.FirstOrDefaultAsync(s => s.Id == data.Id);

            if(existingData == null) return existingData;

            existingData.SkillName = data.SkillName;

            _context.Update(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task DeleteSkillEmployeeBySkillId(string skillId)
        {
            if (string.IsNullOrWhiteSpace(skillId)) return;

            var existingDatas = _context.SkillEmployees.Where(s => s.SkillId == skillId);

            if (existingDatas?.Count() > 0)
            {
                _context.RemoveRange(existingDatas);
            }

            await _context.SaveChangesAsync();
        }
    }
}
