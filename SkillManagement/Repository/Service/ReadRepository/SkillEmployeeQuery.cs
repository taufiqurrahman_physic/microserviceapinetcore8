﻿using Dapper;
using SkillManajement.Context;
using SkillManajement.Repository.Interface.ReadRepository;
using SkillManagement.Entity;

namespace SkillManajement.Repository.Service.ReadRepository
{
    public class SkillEmployeeQuery : ISkillEmployeeQuery
    {
        private readonly DapperContext _context;
        public SkillEmployeeQuery(DapperContext context)
        {
            _context = context;
        }
        public async Task<SkillEmployee> GetSkillEmploye(string id)
        {
            var query = "SELECT * FROM SkillEmployees WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<SkillEmployee>(query, new { id });

                return result;
            }
        }

        public async Task<IEnumerable<SkillEmployee>> GetSkillEmployees()
        {
            var query = "SELECT * FROM SkillEmployees";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<SkillEmployee>(query);
                return result.ToList();
            }
        }
    }
}
