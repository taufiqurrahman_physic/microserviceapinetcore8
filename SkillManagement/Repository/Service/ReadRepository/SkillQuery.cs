﻿using Dapper;
using SkillManajement.Context;
using SkillManajement.Repository.Interface.ReadRepository;
using SkillManagement.Entity;

namespace SkillManajement.Repository.Service.ReadRepository
{
    public class SkillQuery : ISkillQuery
    {
        private readonly DapperContext _context;
        public SkillQuery(DapperContext context)
        {
            _context = context;
        }
        public async Task<Skill> GetSkill(string id)
        {
            var query = "SELECT * FROM Skills WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Skill>(query, new { id });

                return result;
            }
        }

        public async Task<IEnumerable<Skill>> GetSkills()
        {
            var query = "SELECT * FROM Skills";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Skill>(query);
                return result.ToList();
            }
        }
    }
}
