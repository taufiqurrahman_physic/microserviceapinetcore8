﻿using AutoMapper;
using SkillManajement.MediatR.RequestModel.Query.Employe;
using SkillManajement.Repository.Interface.ReadRepository;
using SkillManajement.ViewModel;
using MediatR;
using SkillManajement.InternalApiServices.Interface;

namespace SkillManajement.MediatR.Query
{
    public class SkillEmployeeQueryHandler : IRequestHandler<GetAllSkillEmployees, IEnumerable<SkillEmployeeViewModel>>,
                                        IRequestHandler<GetSkillEmployeeById, SkillEmployeeViewModel>
    {
        private readonly ISkillEmployeeQuery _repository;
        private readonly IMapper _mapper;
        private readonly IEmployeeApiService _apiService;

        public SkillEmployeeQueryHandler(ISkillEmployeeQuery repository,
                                    IMapper mapper,
                                    IEmployeeApiService apiService)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
        }

        public async Task<IEnumerable<SkillEmployeeViewModel>> Handle(GetAllSkillEmployees request, CancellationToken cancellationToken)
        {
            var datas = await _repository.GetSkillEmployees();

            if (datas == null)
            {
                return null;
            }

            var result = datas.Select(e=>_mapper.Map<SkillEmployeeViewModel>(e));

            var employees = await _apiService.GetDatas();

            if (employees == null || !employees.Any())
                return result;

            result = result.Select(s =>
            {
                var item = s;
                item.Employee = employees.FirstOrDefault(i => i.Id == item.EmployeeId);

                return item;
            });

            return result;
        }

        public async Task<SkillEmployeeViewModel> Handle(GetSkillEmployeeById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetSkillEmploye(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<SkillEmployeeViewModel>(data);
            result.Employee = await _apiService.GetDataFromId(request.Id);

            return result;
        }
    }
}
