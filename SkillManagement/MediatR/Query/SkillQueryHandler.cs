﻿using AutoMapper;
using SkillManajement.MediatR.RequestModel.Query.Employe;
using SkillManajement.Repository.Interface.ReadRepository;
using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.Query
{
    public class SkillQueryHandler : IRequestHandler<GetAllSkills, IEnumerable<SkillViewModel>>,
                                        IRequestHandler<GetSkillById, SkillViewModel>
    {
        private readonly ISkillQuery _repository;
        private readonly IMapper _mapper;

        public SkillQueryHandler(ISkillQuery repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SkillViewModel>> Handle(GetAllSkills request, CancellationToken cancellationToken)
        {
            var datas = await _repository.GetSkills();

            if (datas == null)
            {
                return null;
            }

            return datas.Select(e=>_mapper.Map<SkillViewModel>(e));
        }

        public async Task<SkillViewModel> Handle(GetSkillById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetSkill(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<SkillViewModel>(data);

            return result;
        }
    }
}
