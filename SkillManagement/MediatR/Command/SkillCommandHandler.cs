﻿using AutoMapper;
using SkillManajement.MediatR.RequestModel.Command.Employe;
using SkillManajement.Repository.Interface.CommandRepository;
using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;

namespace SkillManajement.MediatR.Query
{
    public class SkillCommandHandler : IRequestHandler<CreateSkill, SkillViewModel>,
                                        IRequestHandler<UpdateSkill, SkillViewModel>,
                                        IRequestHandler<DeleteSkill, SkillViewModel>
    {
        private readonly ISkillCommand _repository;
        private readonly IMapper _mapper;

        public SkillCommandHandler(ISkillCommand repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<SkillViewModel> Handle(CreateSkill request, CancellationToken cancellationToken)
        {
            var data = await _repository.Create((Skill)request);

            if (data == null) return null;

            var result = _mapper.Map<SkillViewModel>(data);

            return result;
        }

        public async Task<SkillViewModel> Handle(UpdateSkill request, CancellationToken cancellationToken)
        {
            var data = await _repository.Update((Skill)request);

            if (data == null) return null;

            var result = _mapper.Map<SkillViewModel>(data);

            return result;
        }

        public async Task<SkillViewModel> Handle(DeleteSkill request, CancellationToken cancellationToken)
        {
            await _repository.DeleteSkillEmployeeBySkillId(request.Id);

            var data = await _repository.Delete(request.Id);

            if (data == null) return null;

            var result = _mapper.Map<SkillViewModel>(data);

            return result;
        }
    }
}
