﻿using AutoMapper;
using SkillManajement.MediatR.RequestModel.Command.Employe;
using SkillManajement.Repository.Interface.CommandRepository;
using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;
using SkillManagement.MediatR.RequestModel.Command.SkillEmployee;

namespace SkillManajement.MediatR.Query
{
    public class SkillEmployeeCommandHandler : IRequestHandler<CreateSkillEmployee, SkillEmployeeViewModel>,
                                        IRequestHandler<UpdateSkillEmployee, SkillEmployeeViewModel>,
                                        IRequestHandler<DeleteSkillEmployee, SkillEmployeeViewModel>,
                                        IRequestHandler<DeleteRecordsByEmployeeId, bool>
    {
        private readonly ISkillEmployeeCommand _repository;
        private readonly IMapper _mapper;

        public SkillEmployeeCommandHandler(ISkillEmployeeCommand repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<SkillEmployeeViewModel> Handle(CreateSkillEmployee request, CancellationToken cancellationToken)
        {
            var data = await _repository.Create((SkillEmployee)request);

            if (data == null) return null;

            var result = _mapper.Map<SkillEmployeeViewModel>(data);

            return result;
        }

        public async Task<SkillEmployeeViewModel> Handle(UpdateSkillEmployee request, CancellationToken cancellationToken)
        {
            var data = await _repository.Update((SkillEmployee)request);

            if (data == null) return null;

            var result = _mapper.Map<SkillEmployeeViewModel>(data);

            return result;
        }

        public async Task<SkillEmployeeViewModel> Handle(DeleteSkillEmployee request, CancellationToken cancellationToken)
        {
            var data = await _repository.Delete(request.Id);

            if (data == null) return null;

            var result = _mapper.Map<SkillEmployeeViewModel>(data);

            return result;
        }

        public async Task<bool> Handle(DeleteRecordsByEmployeeId request, CancellationToken cancellationToken)
        {
            var response = await _repository.DeleteByEmployeeId(request.EmployeeId);

            return response;
        }
    }
}
