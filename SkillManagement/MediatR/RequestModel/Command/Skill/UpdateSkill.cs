﻿using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class UpdateSkill : Skill, IRequest<SkillViewModel>
    {
        public UpdateSkill(string id, string skillName)
        {
            Id = id;
            this.SkillName = skillName;
        }
    }
}
