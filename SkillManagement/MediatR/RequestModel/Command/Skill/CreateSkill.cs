﻿using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class CreateSkill : Skill, IRequest<SkillViewModel>
    {
        public CreateSkill(string skillName)
        {
            this.SkillName = skillName;
        }
    }
}
