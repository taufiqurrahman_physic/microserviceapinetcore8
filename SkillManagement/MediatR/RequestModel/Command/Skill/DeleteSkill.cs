﻿using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteSkill: IRequest<SkillViewModel>
    {
        public string Id { get; set; }

        public DeleteSkill(string id)
        {
            Id = id;
        }
    }
}
