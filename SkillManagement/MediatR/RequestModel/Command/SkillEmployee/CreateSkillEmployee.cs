﻿using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class CreateSkillEmployee : SkillEmployee, IRequest<SkillEmployeeViewModel>
    {
        public CreateSkillEmployee(string skillId, string employeeId)
        {
            this.SkillId = skillId;
            this.EmployeeId = employeeId;
        }
    }
}
