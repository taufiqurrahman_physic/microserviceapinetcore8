﻿using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteSkillEmployee: IRequest<SkillEmployeeViewModel>
    {
        public string Id { get; set; }

        public DeleteSkillEmployee(string id)
        {
            Id = id;
        }
    }
}
