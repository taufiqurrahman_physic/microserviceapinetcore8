﻿using SkillManajement.ViewModel;
using MediatR;
using SkillManagement.Entity;

namespace SkillManajement.MediatR.RequestModel.Command.Employe
{
    public class UpdateSkillEmployee : SkillEmployee, IRequest<SkillEmployeeViewModel>
    {
        public UpdateSkillEmployee(string id, string skilllId, string employeeId)
        {
            Id = id;
            this.SkillId = skilllId;
            this.EmployeeId = employeeId;
        }
    }
}
