﻿using MediatR;

namespace SkillManagement.MediatR.RequestModel.Command.SkillEmployee
{
    public class DeleteRecordsByEmployeeId : IRequest<bool>
    {
        public string EmployeeId { get; set; }

        public DeleteRecordsByEmployeeId(string employeeId)
        {
            EmployeeId = employeeId;
        }
    }
}
