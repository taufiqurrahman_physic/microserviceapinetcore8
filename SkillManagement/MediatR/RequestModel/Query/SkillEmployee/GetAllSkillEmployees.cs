﻿using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.RequestModel.Query.Employe
{
    public class GetAllSkillEmployees: IRequest<IEnumerable<SkillEmployeeViewModel>>
    {

    }
}
