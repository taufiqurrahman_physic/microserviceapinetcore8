﻿using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.RequestModel.Query.Employe
{
    public class GetSkillEmployeeById : IRequest<SkillEmployeeViewModel>
    {
        public string Id { get; set; }

        public GetSkillEmployeeById(string id)
        {
            Id = id;
        }
    }
}
