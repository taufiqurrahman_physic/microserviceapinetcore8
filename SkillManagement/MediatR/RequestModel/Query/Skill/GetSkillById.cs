﻿using SkillManajement.ViewModel;
using MediatR;

namespace SkillManajement.MediatR.RequestModel.Query.Employe
{
    public class GetSkillById : IRequest<SkillViewModel>
    {
        public string Id { get; set; }

        public GetSkillById(string id)
        {
            Id = id;
        }
    }
}
