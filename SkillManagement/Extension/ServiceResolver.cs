﻿using SkillManajement.InternalApiServices.Interface;
using SkillManajement.InternalApiServices.Service;
using SkillManajement.Repository.Interface.CommandRepository;
using SkillManajement.Repository.Interface.ReadRepository;
using SkillManajement.Repository.Service.CommandRepository;
using SkillManajement.Repository.Service.ReadRepository;

namespace SkillManajement.Extension
{
    public static class ServiceResolver
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<ISkillCommand, SkillCommand>();
            services.AddScoped<ISkillQuery, SkillQuery>();
            services.AddScoped<ISkillEmployeeCommand, SkillEmployeeCommand>();
            services.AddScoped<ISkillEmployeeQuery, SkillEmployeeQuery>();
            services.AddScoped<IEmployeeApiService, EmployeeApiService>();
        }
    }
}
