﻿using Microsoft.EntityFrameworkCore;
using SkillManagement.Entity;

namespace SkillManajement.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        {

        }

        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillEmployee> SkillEmployees { get; set; }
    }
}
