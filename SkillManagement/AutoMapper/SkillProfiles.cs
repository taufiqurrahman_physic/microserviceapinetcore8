﻿using AutoMapper;
using SkillManajement.ViewModel;
using SkillManagement.Entity;

namespace SkillManajement.AutoMapper
{
    public class SkillProfiles : Profile
    {
        public SkillProfiles() 
        {
            CreateMap<SkillViewModel, Skill>()
                .ReverseMap();

            CreateMap<SkillEmployeeViewModel, SkillEmployee>()
               .ReverseMap();
        }
    }
}
