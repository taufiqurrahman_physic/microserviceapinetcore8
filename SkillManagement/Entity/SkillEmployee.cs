﻿using System.ComponentModel.DataAnnotations;

namespace SkillManagement.Entity
{
    public class SkillEmployee
    {
        [Key]
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public virtual Skill Skill { get; set; }
        public string SkillId { get; set; }
    }
}
