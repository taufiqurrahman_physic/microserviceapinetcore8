﻿using System.ComponentModel.DataAnnotations;

namespace SkillManagement.Entity
{
    public class Skill
    {
        [Key]
        public string Id { get; set; }
        public string SkillName { get; set; }
        public ICollection<SkillEmployee> SkillEmployees { get; set; }
    }
}
