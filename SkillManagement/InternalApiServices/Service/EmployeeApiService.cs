﻿using EmployeeManajement.ViewModel;
using SkillManajement.InternalApiServices.Interface;
using System.Text.Json;
using ViewModel;

namespace SkillManajement.InternalApiServices.Service
{
    public class EmployeeApiService : IEmployeeApiService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _apiurl { get; set; }
        public EmployeeApiService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            _apiurl = @"https://localhost:5001/fiqapi/employee";
        }

        public async Task<EmployeeViewModel> GetDataFromId(string employeeId)
        {
            var result = new EmployeeViewModel();
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(Path.Combine(_apiurl, employeeId));

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    if (string.IsNullOrEmpty(content))
                    {
                        throw new Exception("Response string is empty");
                    }

                    var message = JsonSerializer.Deserialize<SingleResponseViewModel<EmployeeViewModel>>(content, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    if (message == null)
                    {
                        throw new Exception("Failed Deserialize json string to object");
                    }

                    result = message.Data;
                }
                else
                {
                    throw new Exception($"Failed to call the API. Status code: {response.StatusCode}");
                }
            }
            return result;
        }

        public async Task<IEnumerable<EmployeeViewModel>> GetDatas()
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(Path.Combine(_apiurl));

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    if (string.IsNullOrEmpty(content))
                    {
                        throw new Exception("Response string is empty");
                    }

                    var message = JsonSerializer.Deserialize<ListResponseViewModel<EmployeeViewModel>>(content, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    if (message == null)
                    {
                        throw new Exception("Failed Deserialize json string to object");
                    }

                    return message.Datas;
                }
                else
                {
                    throw new Exception($"Failed to call the API. Status code: {response.StatusCode}");
                }
            }
        }
    }
}
