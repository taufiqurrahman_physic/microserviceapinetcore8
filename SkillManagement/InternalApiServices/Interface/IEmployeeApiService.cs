﻿using EmployeeManajement.ViewModel;

namespace SkillManajement.InternalApiServices.Interface
{
    public interface IEmployeeApiService
    {
        Task<EmployeeViewModel> GetDataFromId(string employeeId);
        Task<IEnumerable<EmployeeViewModel>> GetDatas();
    }
}
