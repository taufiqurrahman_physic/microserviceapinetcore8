﻿using SkillManajement.MediatR.RequestModel.Command.Employe;
using SkillManajement.MediatR.RequestModel.Query.Employe;
using SkillManajement.ViewModel;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using SkillManagement.MediatR.RequestModel.Command.SkillEmployee;
using Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SkillManajement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillEmployeeController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public SkillEmployeeController(IMediator mediator, ILogger<SkillEmployeeController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        // GET: api/<EmployeeController>
        [HttpGet]
        public async Task<ListResponseViewModel<SkillEmployeeViewModel>> Get()
        {
            _logger.LogInformation("Fetch all data of employee");

            var datas = await _mediator.Send(new GetAllSkillEmployees());

            var result = new ListResponseViewModel<SkillEmployeeViewModel>();
            
            if (datas == null)
            {
                _logger.LogInformation("No data of employees");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Datas = new List<SkillEmployeeViewModel>();

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Datas = datas;

            return result;
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public async Task<SingleResponseViewModel<SkillEmployeeViewModel>> Get(string id)
        {
            var result = new SingleResponseViewModel<SkillEmployeeViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Fetch particular employee for this id = "+id);

            var data = await _mediator.Send(new GetSkillEmployeeById(id));

            if(data == null)
            {
                _logger.LogInformation("No data of employee for this id = "+id+" found.");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Data = data;

            return result;
        }

        // POST api/<EmployeeController>
        [HttpPost]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillEmployeeViewModel>> Post([FromBody] BaseSkillEmployeeViewModel data)
        {
            var result = new SingleResponseViewModel<SkillEmployeeViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to insert the data");

            var response = await _mediator.Send(new CreateSkillEmployee(
                data.SkillId,
                data.EmployeeId
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee created");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data created";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "New Record Created.";
            result.Data = response;

            return result;

        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillEmployeeViewModel>> Put(string id, [FromBody] BaseSkillEmployeeViewModel data)
        {
            var result = new SingleResponseViewModel<SkillEmployeeViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to update the data");

            var response = await _mediator.Send(new UpdateSkillEmployee(
                id,
                data.SkillId,
                data.EmployeeId
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee updated");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data updated";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Data updated.";
            result.Data = response;

            return result;
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillEmployeeViewModel>> Delete(string id)
        {
            var result = new SingleResponseViewModel<SkillEmployeeViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Parameter id is empty or null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteSkillEmployee(
            id));

            if (response == null)
            {
                _logger.LogInformation("No data of employee deleted");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data daleted";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "This data removed from database.";
            result.Data = response;

            return result;
        }

        [HttpDelete("DeleteByEmployeeId")]
        public async Task<BooleanResponseViewModel> DeleteByEmployeeId([FromQuery]string employeeId)
        {
            var result = new BooleanResponseViewModel();

            if (string.IsNullOrEmpty(employeeId))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Failed remove data from database.";
                result.IsSuccess = false;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteRecordsByEmployeeId(
            employeeId));

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = response ? "This data removed from database.":"Failed remove data from database.";
            result.IsSuccess = response;

            return result;
        }
    }
}
