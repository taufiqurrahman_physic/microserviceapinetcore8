﻿using SkillManajement.MediatR.RequestModel.Command.Employe;
using SkillManajement.MediatR.RequestModel.Query.Employe;
using SkillManajement.ViewModel;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using Models.ViewModels;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SkillManajement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public SkillController(IMediator mediator, ILogger<SkillController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }



        // GET: api/<EmployeeController>
        [HttpGet]
        public async Task<ListResponseViewModel<SkillViewModel>> Get()
        {
            _logger.LogInformation("Fetch all data of employee");

            var datas = await _mediator.Send(new GetAllSkills());

            var result = new ListResponseViewModel<SkillViewModel>();
            
            if (datas == null)
            {
                _logger.LogInformation("No data of employees");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Datas = new List<SkillViewModel>();

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Datas = datas;

            return result;
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public async Task<SingleResponseViewModel<SkillViewModel>> Get(string id)
        {
            var result = new SingleResponseViewModel<SkillViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Fetch particular employee for this id = "+id);

            var data = await _mediator.Send(new GetSkillById(id));

            if(data == null)
            {
                _logger.LogInformation("No data of employee for this id = "+id+" found.");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Data = data;

            return result;
        }

        // POST api/<EmployeeController>
        [HttpPost]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillViewModel>> Post([FromBody] BaseSkillViewModel data)
        {
            var result = new SingleResponseViewModel<SkillViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to insert the data");

            var response = await _mediator.Send(new CreateSkill(
                data.SkillName
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee created");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data created";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "New Record Created.";
            result.Data = response;

            return result;

        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillViewModel>> Put(string id, [FromBody] BaseSkillViewModel data)
        {
            var result = new SingleResponseViewModel<SkillViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to update the data");

            var response = await _mediator.Send(new UpdateSkill(
                id,
                data.SkillName
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee updated");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data updated";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Data updated.";
            result.Data = response;

            return result;
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<SkillViewModel>> Delete(string id)
        {
            var result = new SingleResponseViewModel<SkillViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Parameter id is empty or null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteSkill(
            id));

            if (response == null)
            {
                _logger.LogInformation("No data of employee deleted");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data daleted";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "This data removed from database.";
            result.Data = response;

            return result;
        }
    }
}
