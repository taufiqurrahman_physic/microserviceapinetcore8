﻿using AccountManajement.ViewModel;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Models.ViewModels
{
    public class BaseEmployeeViewModel
    {
        [JsonProperty(PropertyName = "name")]
        [Required(ErrorMessage = "This field is required.")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "date_of_birth")]
        [Required(ErrorMessage = "This field is required.")]
        public DateTime DateOfBirth { get; set; }

        [JsonProperty(PropertyName = "address")]
        [Required(ErrorMessage = "This field is required.")]
        public string Address { get; set; }

        [JsonProperty(PropertyName = "account_id")]
        [Required(ErrorMessage = "This field is required.")]
        public string AccountId { get; set; }
    }
}
