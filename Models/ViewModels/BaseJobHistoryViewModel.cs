﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Models.ViewModels
{
    public class BaseJobHistoryViewModel
    {
        [JsonProperty(PropertyName = "employee_id")]
        [Required(ErrorMessage = "This field is required.")]
        public string EmployeeId { get; set; }

        [JsonProperty(PropertyName = "position")]
        [Required(ErrorMessage = "This field is required.")]
        public string Position { get; set; }

        [JsonProperty(PropertyName = "company_name")]
        [Required(ErrorMessage = "This field is required.")]
        public string CompanyName { get; set; }

        [JsonProperty(PropertyName = "starting_date")]
        [Required(ErrorMessage = "This field is required.")]
        public DateTime StartingDate { get; set; }

        [JsonProperty(PropertyName = "end_date")]
        [Required(ErrorMessage = "This field is required.")]
        public DateTime EndDate { get; set; }
    }
}
