﻿using AccountManajement.ViewModel;
using Models.ViewModels;
using Newtonsoft.Json;

namespace EmployeeManajement.ViewModel
{
    public class EmployeeViewModel : BaseEmployeeViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "account")]
        public AccountViewModel Account { get; set; }
    }
}
