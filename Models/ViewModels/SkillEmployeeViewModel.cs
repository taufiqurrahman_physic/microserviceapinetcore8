﻿using EmployeeManajement.ViewModel;
using Models.ViewModels;
using Newtonsoft.Json;

namespace SkillManajement.ViewModel
{
    public class SkillEmployeeViewModel : BaseSkillEmployeeViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "skill")]
        public SkillViewModel Skill { get; set; }
        [JsonProperty(PropertyName = "employee")]
        public EmployeeViewModel Employee { get; set; }
    }
    
}
