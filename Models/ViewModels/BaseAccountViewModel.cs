﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Models.ViewModels
{
    public class BaseAccountViewModel
    {
        [JsonProperty(PropertyName = "username")]
        [Required(ErrorMessage = "This field is required.")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "password")]
        [Required(ErrorMessage = "This field is required.")]
        public string Password { get; set; }

        [JsonProperty(PropertyName = "role")]
        [EnumDataType(typeof(UserRole))]
        [Required(ErrorMessage = "This field is required.")]
        public string Role { get; set; }
    }
    public enum UserRole
    {
        [EnumMember(Value = UserRoleName.Admin)]
        Admin,

        [EnumMember(Value = UserRoleName.User)]
        User,
    }

    public class UserRoleName
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
