﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Models.ViewModels
{
    public class BaseSkillViewModel
    {
        [JsonProperty(PropertyName = "skill_name")]
        [Required(ErrorMessage = "This field is required.")]
        public string SkillName { get; set; }
    }
}
