﻿using Models.ViewModels;
using Newtonsoft.Json;

namespace SkillManajement.ViewModel
{
    public class SkillViewModel : BaseSkillViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
