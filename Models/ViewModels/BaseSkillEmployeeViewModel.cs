﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Models.ViewModels
{
    public class BaseSkillEmployeeViewModel
    {
        [JsonProperty(PropertyName = "skill_id")]
        [Required(ErrorMessage = "This field is required.")]
        public string SkillId { get; set; }

        [JsonProperty(PropertyName = "employee_id")]
        [Required(ErrorMessage = "This field is required.")]
        public string EmployeeId { get; set; }
    }
}
