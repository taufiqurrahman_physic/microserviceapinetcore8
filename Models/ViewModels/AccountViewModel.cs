﻿using Models.ViewModels;
using Newtonsoft.Json;

namespace AccountManajement.ViewModel
{
    public class AccountViewModel : BaseAccountViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
