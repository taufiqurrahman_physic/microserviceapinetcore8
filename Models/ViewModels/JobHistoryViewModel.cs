﻿using EmployeeManajement.ViewModel;
using Models.ViewModels;
using Newtonsoft.Json;

namespace JobHistoryManajement.ViewModel
{
    public class JobHistoryViewModel : BaseJobHistoryViewModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "employee")]
        public EmployeeViewModel Employee { get; set; }
    }
}
