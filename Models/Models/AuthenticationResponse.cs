﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AuthenticationResponse
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        [JsonProperty(PropertyName = "jwt_token")]
        public string JwtToken { get; set; }
        [JsonProperty(PropertyName = "ExpiresIn")]
        public int ExpiresIn { get; set; }
    }
}
