﻿using System.ComponentModel.DataAnnotations;

namespace JobHistoriesManagement.Entity
{
    public class JobHistory
    {
        [Key]
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public DateTime StartingDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
