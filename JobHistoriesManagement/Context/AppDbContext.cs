﻿using JobHistoriesManagement.Entity;
using Microsoft.EntityFrameworkCore;

namespace JobHistoryManajement.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        {

        }

        public DbSet<JobHistory> JobHistories { get; set; }
    }
}
