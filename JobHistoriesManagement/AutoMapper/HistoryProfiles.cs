﻿using AutoMapper;
using JobHistoryManajement.ViewModel;
using JobHistoriesManagement.Entity;

namespace JobHistoryManajement.AutoMapper
{
    public class HistoryProfiles : Profile
    {
        public HistoryProfiles() 
        {
            CreateMap<JobHistoryViewModel, JobHistory>()
                .ReverseMap();
        }
    }
}
