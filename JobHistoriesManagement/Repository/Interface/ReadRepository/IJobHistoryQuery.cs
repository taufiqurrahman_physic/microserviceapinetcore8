﻿using JobHistoriesManagement.Entity;

namespace JobHistoryManajement.Repository.Interface.ReadRepository
{
    public interface IJobHistoryQuery
    {
        Task<IEnumerable<JobHistory>> GetJobHistories();
        Task<JobHistory> GetJobHistory(string id);
    }
}
