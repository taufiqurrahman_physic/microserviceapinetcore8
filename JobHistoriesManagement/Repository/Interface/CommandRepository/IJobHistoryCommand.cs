﻿using JobHistoriesManagement.Entity;

namespace JobHistoryManajement.Repository.Interface.CommandRepository
{
    public interface IJobHistoryCommand
    {
        Task<JobHistory> Create(JobHistory data);
        Task<JobHistory> Update(JobHistory data);
        Task<JobHistory> Delete(string id);
        Task<bool> DeleteByEmployeeId(string employeeId);
    }
}
