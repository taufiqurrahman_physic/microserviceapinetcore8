﻿using JobHistoryManajement.Context;
using JobHistoryManajement.Repository.Interface.CommandRepository;
using JobHistoriesManagement.Entity;
using Microsoft.EntityFrameworkCore;

namespace JobHistoryManajement.Repository.Service.CommandRepository
{
    public class JobHistoryCommand : IJobHistoryCommand
    {
        private readonly AppDbContext _context;

        public JobHistoryCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<JobHistory> Create(JobHistory data)
        {
            if (data == null)
                return data;

            data.Id = Guid.NewGuid().ToString();

            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }

        public async Task<JobHistory> Delete(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return null;

            var existingData = await _context.JobHistories.FirstOrDefaultAsync(s=>s.Id == id);

            if(existingData == null) return existingData;

            _context.Remove(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task<bool> DeleteByEmployeeId(string employeeId)
        {
            if (string.IsNullOrWhiteSpace(employeeId)) return false;

            var existingData = _context.JobHistories.Where(s => s.EmployeeId == employeeId);

            if (existingData == null) return false;

            _context.RemoveRange(existingData);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<JobHistory> Update(JobHistory data)
        {
            if (data == null) return data;

            var existingData = await _context.JobHistories.FirstOrDefaultAsync(s => s.Id == data.Id);

            if(existingData == null) return existingData;

            existingData.CompanyName = data.CompanyName;
            existingData.StartingDate = data.StartingDate;
            existingData.EndDate = data.EndDate;
            existingData.Position = data.Position;
            existingData.EmployeeId = data.EmployeeId;

            _context.Update(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }
    }
}
