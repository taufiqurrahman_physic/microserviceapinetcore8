﻿using Dapper;
using JobHistoryManajement.Context;
using JobHistoryManajement.Repository.Interface.ReadRepository;
using JobHistoriesManagement.Entity;

namespace JobHistoryManajement.Repository.Service.ReadRepository
{
    public class JobHistoryQuery : IJobHistoryQuery
    {
        private readonly DapperContext _context;
        public JobHistoryQuery(DapperContext context)
        {
            _context = context;
        }
        public async Task<JobHistory> GetJobHistory(string id)
        {
            var query = "SELECT * FROM JobHistories WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<JobHistory>(query, new { id });

                return result;
            }
        }

        public async Task<IEnumerable<JobHistory>> GetJobHistories()
        {
            var query = "SELECT * FROM JobHistories";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<JobHistory>(query);
                return result.ToList();
            }
        }
    }
}
