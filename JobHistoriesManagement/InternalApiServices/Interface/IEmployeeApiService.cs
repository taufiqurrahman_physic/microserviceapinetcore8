﻿using EmployeeManajement.ViewModel;

namespace JobHistoriesManagement.InternalApiServices.Interface
{
    public interface IEmployeeApiService
    {
        Task<EmployeeViewModel> GetDataFromId(string employeeId);
        Task<IEnumerable<EmployeeViewModel>> GetDatas();
    }
}
