﻿using JobHistoryManajement.ViewModel;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Query.Employe
{
    public class GetAllJobHistories: IRequest<IEnumerable<JobHistoryViewModel>>
    {

    }
}
