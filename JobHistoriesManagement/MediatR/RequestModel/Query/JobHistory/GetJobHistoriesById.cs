﻿using JobHistoryManajement.ViewModel;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Query.Employe
{
    public class GetJobHistoriesById : IRequest<JobHistoryViewModel>
    {
        public string Id { get; set; }

        public GetJobHistoriesById(string id)
        {
            Id = id;
        }
    }
}
