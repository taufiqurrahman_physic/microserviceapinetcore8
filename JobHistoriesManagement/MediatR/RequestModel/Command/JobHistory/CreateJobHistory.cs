﻿using JobHistoryManajement.ViewModel;
using JobHistoriesManagement.Entity;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Command.Employe
{
    public class CreateJobHistory : JobHistory, IRequest<JobHistoryViewModel>
    {
        public CreateJobHistory(string employeeId,DateTime startingDate, DateTime endDate, string position, string companyName)
        {
            this.EmployeeId = employeeId;
            this.EndDate = endDate;
            this.StartingDate = startingDate;
            this.Position = position;
            this.CompanyName = companyName;
        }
    }
}
