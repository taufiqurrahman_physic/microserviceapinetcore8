﻿using JobHistoryManajement.ViewModel;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteJobHistory: IRequest<JobHistoryViewModel>
    {
        public string Id { get; set; }

        public DeleteJobHistory(string id)
        {
            Id = id;
        }
    }
}
