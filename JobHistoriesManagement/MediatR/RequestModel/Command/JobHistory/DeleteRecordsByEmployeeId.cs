﻿using JobHistoryManajement.ViewModel;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteRecordsByEmployeeId : IRequest<bool>
    {
        public string EmployeeId { get; set; }

        public DeleteRecordsByEmployeeId(string employeeId)
        {
            EmployeeId = employeeId;
        }
    }
}
