﻿using JobHistoryManajement.ViewModel;
using JobHistoriesManagement.Entity;
using MediatR;

namespace JobHistoryManajement.MediatR.RequestModel.Command.Employe
{
    public class UpdateJobHistory : JobHistory, IRequest<JobHistoryViewModel>
    {
        public UpdateJobHistory(string id, string employeeId, DateTime startingDate, DateTime endDate, string position, string companyName)
        {
            Id = id;
            this.EmployeeId = employeeId;
            this.EndDate = endDate;
            this.StartingDate = startingDate;
            this.Position = position;
            this.CompanyName = companyName;
        }
    }
}
