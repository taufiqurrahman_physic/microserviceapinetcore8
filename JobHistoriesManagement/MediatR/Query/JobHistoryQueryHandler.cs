﻿using AutoMapper;
using JobHistoriesManagement.InternalApiServices.Interface;
using JobHistoryManajement.MediatR.RequestModel.Query.Employe;
using JobHistoryManajement.Repository.Interface.ReadRepository;
using JobHistoryManajement.ViewModel;
using MediatR;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace JobHistoryManajement.MediatR.Query
{
    public class JobHistoryQueryHandler : IRequestHandler<GetAllJobHistories, IEnumerable<JobHistoryViewModel>>,
                                        IRequestHandler<GetJobHistoriesById, JobHistoryViewModel>
    {
        private readonly IJobHistoryQuery _repository;
        private readonly IMapper _mapper;
        private readonly IEmployeeApiService _apiService;

        public JobHistoryQueryHandler(IJobHistoryQuery repository,
                                    IMapper mapper,
                                    IEmployeeApiService apiService)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
        }

        public async Task<IEnumerable<JobHistoryViewModel>> Handle(GetAllJobHistories request, CancellationToken cancellationToken)
        {
            
            var datas = await _repository.GetJobHistories();

            if (datas == null || !datas.Any())
            {
                return null;
            }

            var result =  datas.Select(e=>_mapper.Map<JobHistoryViewModel>(e));

            var employees = await _apiService.GetDatas();

            if (employees == null || !employees.Any())
                return result;

            result = result.Select(s =>
            {
                var item = s;
                item.Employee = employees.FirstOrDefault(i => i.Id == item.EmployeeId);

                return item;
            });

            return result;
        }

        public async Task<JobHistoryViewModel> Handle(GetJobHistoriesById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetJobHistory(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<JobHistoryViewModel>(data);
            result.Employee = await _apiService.GetDataFromId(data.EmployeeId);

            return result;
        }
    }
}
