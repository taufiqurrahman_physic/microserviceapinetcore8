﻿using AutoMapper;
using JobHistoryManajement.MediatR.RequestModel.Command.Employe;
using JobHistoryManajement.Repository.Interface.CommandRepository;
using JobHistoryManajement.ViewModel;
using JobHistoriesManagement.Entity;
using MediatR;
using JobHistoriesManagement.InternalApiServices.Interface;

namespace JobHistoryManajement.MediatR.Query
{
    public class JobHistoryCommandHandler : IRequestHandler<CreateJobHistory, JobHistoryViewModel>,
                                        IRequestHandler<UpdateJobHistory, JobHistoryViewModel>,
                                        IRequestHandler<DeleteJobHistory, JobHistoryViewModel>,
                                        IRequestHandler<DeleteRecordsByEmployeeId, bool>
    {
        private readonly IJobHistoryCommand _repository;
        private readonly IMapper _mapper;
        private readonly IEmployeeApiService _apiService;

        public JobHistoryCommandHandler(IJobHistoryCommand repository,
                                    IMapper mapper,
                                    IEmployeeApiService apiService)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
        }

        public async Task<JobHistoryViewModel> Handle(CreateJobHistory request, CancellationToken cancellationToken)
        {
            var employee = await _apiService.GetDataFromId(request.EmployeeId);

            if(employee == null)
            {
                throw new Exception("Employee not found");
            }

            var data = await _repository.Create((JobHistory)request);

            if (data == null) throw new Exception("Failed create new data");

            var result = _mapper.Map<JobHistoryViewModel>(data);
            result.Employee = employee;

            return result;
        }

        public async Task<JobHistoryViewModel> Handle(UpdateJobHistory request, CancellationToken cancellationToken)
        {
            var employee = await _apiService.GetDataFromId(request.EmployeeId);

            if (employee == null)
            {
                throw new Exception("Employee not found");
            }

            var data = await _repository.Update((JobHistory)request);

            if (data == null) throw new Exception("Failed modify this data");

            var result = _mapper.Map<JobHistoryViewModel>(data);
            result.Employee = employee;

            return result;
        }

        public async Task<JobHistoryViewModel> Handle(DeleteJobHistory request, CancellationToken cancellationToken)
        {
            var data = await _repository.Delete(request.Id);

            if (data == null) throw new Exception("Failed delete this data");

            var result = _mapper.Map<JobHistoryViewModel>(data);

            return result;
        }

        public async Task<bool> Handle(DeleteRecordsByEmployeeId request, CancellationToken cancellationToken)
        {
            var response = await _repository.DeleteByEmployeeId(request.EmployeeId);

            return response;
        }
    }
}
