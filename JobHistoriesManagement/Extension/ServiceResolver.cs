﻿using JobHistoriesManagement.InternalApiServices.Interface;
using JobHistoriesManagement.InternalApiServices.Service;
using JobHistoryManajement.Repository.Interface.CommandRepository;
using JobHistoryManajement.Repository.Interface.ReadRepository;
using JobHistoryManajement.Repository.Service.CommandRepository;
using JobHistoryManajement.Repository.Service.ReadRepository;

namespace JobHistoryManajement.Extension
{
    public static class ServiceResolver
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IJobHistoryCommand, JobHistoryCommand>();
            services.AddScoped<IJobHistoryQuery, JobHistoryQuery>();
            services.AddScoped<IEmployeeApiService, EmployeeApiService>();
        }
    }
}
