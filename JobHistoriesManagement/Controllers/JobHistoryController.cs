﻿using JobHistoryManajement.MediatR.RequestModel.Command.Employe;
using JobHistoryManajement.MediatR.RequestModel.Query.Employe;
using JobHistoryManajement.ViewModel;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.ViewModels;
using ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JobHistoryManajement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobHistoryController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;

        public JobHistoryController(IMediator mediator, ILogger<JobHistoryController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }



        // GET: api/<EmployeeController>
        [HttpGet]
        public async Task<ListResponseViewModel<JobHistoryViewModel>> Get()
        {
            _logger.LogInformation("Fetch all data of employee");

            var datas = await _mediator.Send(new GetAllJobHistories());

            var result = new ListResponseViewModel<JobHistoryViewModel>();
            
            if (datas == null)
            {
                _logger.LogInformation("No data of employees");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Datas = new List<JobHistoryViewModel>();

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Datas = datas;

            return result;
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public async Task<SingleResponseViewModel<JobHistoryViewModel>> Get(string id)
        {
            var result = new SingleResponseViewModel<JobHistoryViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Fetch particular employee for this id = "+id);

            var data = await _mediator.Send(new GetJobHistoriesById(id));

            if(data == null)
            {
                _logger.LogInformation("No data of employee for this id = "+id+" found.");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "Data not found";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Success";
            result.Data = data;

            return result;
        }

        // POST api/<EmployeeController>
        [HttpPost]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<JobHistoryViewModel>> Post([FromBody] BaseJobHistoryViewModel data)
        {
            var result = new SingleResponseViewModel<JobHistoryViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to insert the data");

            var response = await _mediator.Send(new CreateJobHistory(
                data.EmployeeId,
                data.StartingDate,
                data.EndDate,
                data.Position,
                data.CompanyName
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee created");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data created";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "New Record Created.";
            result.Data = response;

            return result;

        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<JobHistoryViewModel>> Put(string id, [FromBody] BaseJobHistoryViewModel data)
        {
            var result = new SingleResponseViewModel<JobHistoryViewModel>();

            if (data == null)
            {
                _logger.LogInformation("Data parameter is null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is null";
                result.Data = null;

                return result;
            }

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("Data parameter is is not valid");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Data parameter is is not valid";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to update the data");

            var response = await _mediator.Send(new UpdateJobHistory(
                id,
                data.EmployeeId,
                data.StartingDate,
                data.EndDate,
                data.Position,
                data.CompanyName
                ));

            if (response == null)
            {
                _logger.LogInformation("No data of employee updated");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data updated";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "Data updated.";
            result.Data = response;

            return result;
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoleName.Admin + "," + UserRoleName.User)]
        public async Task<SingleResponseViewModel<JobHistoryViewModel>> Delete(string id)
        {
            var result = new SingleResponseViewModel<JobHistoryViewModel>();

            if (string.IsNullOrEmpty(id))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Parameter id is empty or null";
                result.Data = null;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteJobHistory(
            id));

            if (response == null)
            {
                _logger.LogInformation("No data of employee deleted");

                result.StatusCode = StatusCodes.Status204NoContent;
                result.Message = "No data daleted";
                result.Data = null;

                return result;
            }

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = "This data removed from database.";
            result.Data = response;

            return result;
        }

        [HttpDelete("DeleteByEmployeeId")]
        public async Task<BooleanResponseViewModel> DeleteByEmployeeId([FromQuery] string employeeId)
        {
            var result = new BooleanResponseViewModel();

            if (string.IsNullOrEmpty(employeeId))
            {
                _logger.LogInformation("Id parameter is empty or null");

                result.StatusCode = StatusCodes.Status400BadRequest;
                result.Message = "Failed remove data from database.";
                result.IsSuccess = false;

                return result;
            }

            _logger.LogInformation("Start to delete the data");

            var response = await _mediator.Send(new DeleteRecordsByEmployeeId(
            employeeId));

            result.StatusCode = StatusCodes.Status200OK;
            result.Message = response ? "This data removed from database." : "Failed remove data from database.";
            result.IsSuccess = response;

            return result;
        }
    }
}
