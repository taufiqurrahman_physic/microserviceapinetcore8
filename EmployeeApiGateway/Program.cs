using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using JwtAuthenticationManager;

var builder = WebApplication.CreateBuilder(args);

// Add Ocelot configuration from ocelot.json
builder.Configuration.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true);

builder.Services.AddOcelot(builder.Configuration);
builder.Services.AddCustomJwtAuthentication();

var app = builder.Build();

// Ensure Ocelot is configured and middleware is applied
app.UseOcelot().Wait();

app.UseAuthentication();
app.UseAuthorization();
// Define your routes and APIs below
app.MapGet("/", () => "Hello World!");

app.Run();