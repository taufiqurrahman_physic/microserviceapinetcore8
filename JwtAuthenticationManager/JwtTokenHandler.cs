﻿using System.Text;

namespace JwtAuthenticationManager
{
    public class JwtTokenHandler
    {
        private const string JWT_SECURITY_TOKEN = "8A2F04BDB512E9F1C938313ACB25EAE0B373F40ACDF5D123EF02B13C9C57F1D5";
        public const int JWT_TOKEN_VALIDATION = 20;

        public static string GetHexadecimalStringToken()
        {
            return JWT_SECURITY_TOKEN;
        }

        private static string StringToHex(string input)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            return BitConverter.ToString(bytes).Replace("-", "");
        }
    }
}
