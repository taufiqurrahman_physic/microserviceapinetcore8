﻿using AutoMapper;
using EmployeeManajement.Entity;
using EmployeeManajement.ViewModel;

namespace EmployeeManajement.AutoMapper
{
    public class EmployeeProfiles : Profile
    {
        public EmployeeProfiles() 
        {
            CreateMap<EmployeeViewModel, Employee>()
                .ReverseMap();
        }
    }
}
