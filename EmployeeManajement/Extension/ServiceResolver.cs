﻿using EmployeeManajement.InternalApiServices.Interface;
using EmployeeManajement.InternalApiServices.Service;
using EmployeeManajement.Repository.Interface.CommandRepository;
using EmployeeManajement.Repository.Interface.ReadRepository;
using EmployeeManajement.Repository.Service.CommandRepository;
using EmployeeManajement.Repository.Service.ReadRepository;

namespace EmployeeManajement.Extension
{
    public static class ServiceResolver
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IEmployeCommand, EmployeCommand>();
            services.AddScoped<IEmployeQuery, EmployeQuery>();
            services.AddScoped<IAccountApiService, AccountApiService>();
            services.AddScoped<IJobHistoryApiService, JobHistoryApiService>();
            services.AddScoped<ISkillEmployeeApiService, SkillEmployeeApiService>();
        }
    }
}
