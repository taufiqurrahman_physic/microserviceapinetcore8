﻿using AccountManajement.ViewModel;
using Azure.Core;
using EmployeeManajement.InternalApiServices.Interface;
using System.Text.Json;
using ViewModel;

namespace EmployeeManajement.InternalApiServices.Service
{
    public class AccountApiService : IAccountApiService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _apiurl { get; set; }
        public AccountApiService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            _apiurl = @"https://localhost:5001/fiqapi/account";
        }

        public async Task<AccountViewModel> GetAccountFromId(string accountId)
        {
            var result = new AccountViewModel();
            using (HttpClient client = new HttpClient())
            {
                //string accessToken = GetAccessTokenFromHttpContext(_httpContextAccessor.HttpContext);
                //
                //if (string.IsNullOrEmpty(accessToken))
                //{
                //    return result;
                //}
                //
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.
                //    AuthenticationHeaderValue("Bearer", accessToken);

                HttpResponseMessage response = await client.GetAsync(Path.Combine(_apiurl ,accountId));

                 if (response.IsSuccessStatusCode)
                 {
                     string content = await response.Content.ReadAsStringAsync();

                     if(string.IsNullOrEmpty(content))
                     {
                         throw new Exception("Response string is empty");
                     }

                     var message = JsonSerializer.Deserialize<SingleResponseViewModel<AccountViewModel>>(content, new JsonSerializerOptions
                     {
                         PropertyNameCaseInsensitive = true
                     });

                     if(message == null)
                     {
                        throw new Exception("Failed Deserialize json string to object");
                     }

                    result = message.Data;
                }
                 else
                 {
                     throw new Exception($"Failed to call the API. Status code: {response.StatusCode}");
                 }
            }
            return result;
        }


        public async Task<IEnumerable<AccountViewModel>> GetAccounts()
        {
            using (HttpClient client = new HttpClient())
            {
                //string accessToken = GetAccessTokenFromHttpContext(_httpContextAccessor.HttpContext);
                //
                //if (string.IsNullOrEmpty(accessToken))
                //{
                //    return null;
                //}

                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.
                //    AuthenticationHeaderValue("Bearer", accessToken);

                HttpResponseMessage response = await client.GetAsync(Path.Combine(_apiurl));

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    if (string.IsNullOrEmpty(content))
                    {
                        throw new Exception("Response string is empty");
                    }

                    var message = JsonSerializer.Deserialize<ListResponseViewModel<AccountViewModel>>(content, new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });

                    if (message == null)
                    {
                        throw new Exception("Failed Deserialize json string to object");
                    }

                    return message.Datas;
                }
                else
                {
                    throw new Exception($"Failed to call the API. Status code: {response.StatusCode}");
                }
            }
        }

        private string GetAccessTokenFromHttpContext(HttpContext httpContext)
        {
            if (httpContext == null)
                return string.Empty;
            // Retrieve the Authorization header from the request
            string authorizationHeader = httpContext?.Request.Headers["Authorization"];

            // Check if the Authorization header is present and formatted correctly
            if (!string.IsNullOrEmpty(authorizationHeader) && authorizationHeader.StartsWith("Bearer "))
            {
                // Extract the token from the Authorization header
                return authorizationHeader.Substring("Bearer ".Length);
            }

            return null;
        }
    }
}
