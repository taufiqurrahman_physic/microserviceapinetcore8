﻿using EmployeeManajement.InternalApiServices.Interface;

namespace EmployeeManajement.InternalApiServices.Service
{
    public class JobHistoryApiService : IJobHistoryApiService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _apiurl { get; set; }
        public JobHistoryApiService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            _apiurl = @"https://localhost:5001/fiqapi/jobhistory";
        }

        public async Task<bool> DeleteDatasByEmplyeeId(string employeeId)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.DeleteAsync(Path.Combine(_apiurl, "deleteByEmployeeId?employeeId=" + employeeId));

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    throw new Exception($"Failed to send DELETE Job History table. Status code: {response.StatusCode}");
                }
            }
        }

        private string GetAccessTokenFromHttpContext(HttpContext httpContext)
        {
            if (httpContext == null)
                return string.Empty;
            // Retrieve the Authorization header from the request
            string authorizationHeader = httpContext?.Request.Headers["Authorization"];

            // Check if the Authorization header is present and formatted correctly
            if (!string.IsNullOrEmpty(authorizationHeader) && authorizationHeader.StartsWith("Bearer "))
            {
                // Extract the token from the Authorization header
                return authorizationHeader.Substring("Bearer ".Length);
            }

            return null;
        }
    }
}
