﻿namespace EmployeeManajement.InternalApiServices.Interface
{
    public interface IJobHistoryApiService
    {
        Task<bool> DeleteDatasByEmplyeeId(string employeeId);
    }
}
