﻿using AccountManajement.ViewModel;
using System.Collections.Generic;

namespace EmployeeManajement.InternalApiServices.Interface
{
    public interface IAccountApiService
    {
        Task<AccountViewModel> GetAccountFromId(string accountId);
        Task<IEnumerable<AccountViewModel>> GetAccounts();
    }
}
