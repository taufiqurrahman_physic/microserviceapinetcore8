﻿namespace EmployeeManajement.InternalApiServices.Interface
{
    public interface ISkillEmployeeApiService
    {
        Task<bool> DeleteDatasByEmplyeeId(string employeeId);
    }
}
