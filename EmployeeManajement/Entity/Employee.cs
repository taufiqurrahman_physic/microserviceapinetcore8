﻿using System.ComponentModel.DataAnnotations;

namespace EmployeeManajement.Entity
{
    public class Employee
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string AccountId { get; set; }
    }
}
