﻿using EmployeeManajement.Entity;

namespace EmployeeManajement.Repository.Interface.ReadRepository
{
    public interface IEmployeQuery
    {
        Task<IEnumerable<Employee>> GetEmployees();
        Task<Employee> GetEmploye(string id);
        Task<bool> IsAccountIdExist(string accountId);
        Task<bool> IsAccountIdExist(string accountId, string id);
    }
}
