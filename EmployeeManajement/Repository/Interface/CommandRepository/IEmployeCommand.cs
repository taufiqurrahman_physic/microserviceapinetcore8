﻿using EmployeeManajement.Entity;

namespace EmployeeManajement.Repository.Interface.CommandRepository
{
    public interface IEmployeCommand
    {
        Task<Employee> Create(Employee data);
        Task<Employee> Update(Employee data);
        Task<Employee> Delete(string id);
        Task<IEnumerable<string>> DeleteByAccountId(string accountId);
    }
}
