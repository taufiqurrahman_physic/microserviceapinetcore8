﻿using Dapper;
using EmployeeManajement.Context;
using EmployeeManajement.Entity;
using EmployeeManajement.Repository.Interface.ReadRepository;

namespace EmployeeManajement.Repository.Service.ReadRepository
{
    public class EmployeQuery : IEmployeQuery
    {
        private readonly DapperContext _context;
        public EmployeQuery(DapperContext context)
        {
            _context = context;
        }
        public async Task<Employee> GetEmploye(string id)
        {
            var query = "SELECT * FROM Employees WHERE Id = @Id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Employee>(query, new { id });

                return result;
            }
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            var query = "SELECT * FROM Employees";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QueryAsync<Employee>(query);
                return result.ToList();
            }
        }

        public async Task<bool> IsAccountIdExist(string accountId)
        {
            var query = "SELECT TOP 1 * FROM Employees WHERE AccountId = @accountId";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Employee>(query, new { accountId = accountId});
                return result != null;
            }
        }

        public async Task<bool> IsAccountIdExist(string accountId, string id)
        {
            var query = "SELECT TOP 1 * FROM Employees WHERE AccountId = @accountId AND Id != @id";
            using (var connection = _context.CreateConnection())
            {
                var result = await connection.QuerySingleOrDefaultAsync<Employee>(query, new { accountId = accountId, id = id});
                return result != null;
            }
        }
    }
}
