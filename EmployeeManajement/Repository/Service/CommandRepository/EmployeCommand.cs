﻿using EmployeeManajement.Context;
using EmployeeManajement.Entity;
using EmployeeManajement.Repository.Interface.CommandRepository;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManajement.Repository.Service.CommandRepository
{
    public class EmployeCommand : IEmployeCommand
    {
        private readonly AppDbContext _context;

        public EmployeCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Employee> Create(Employee data)
        {
            if (data == null)
                return null;

            if(await _context.Employees.AnyAsync(e=>e.AccountId == data.AccountId))
            {
                return null;
            }

            data.Id = Guid.NewGuid().ToString();

            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }

        public async Task<Employee> Delete(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return null;

            var existingData = await _context.Employees.FirstOrDefaultAsync(s=>s.Id == id);

            if(existingData == null) return existingData;

            _context.Remove(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task<IEnumerable<string>> DeleteByAccountId(string accountId)
        {
            if (string.IsNullOrWhiteSpace(accountId)) return null;

            var existingDatas = _context.Employees.Where(s => s.AccountId == accountId);

            if (existingDatas == null) return null;

            var result = existingDatas.Select(s => s.Id);

            _context.RemoveRange(existingDatas);
            await _context.SaveChangesAsync();

            return result;
        }

        public async Task<Employee> Update(Employee data)
        {
            if (data == null) return null;

            if (await _context.Employees.AnyAsync(e => e.AccountId == data.AccountId && e.Id != data.Id))
            {
                return null;
            }

            var existingData = await _context.Employees.FirstOrDefaultAsync(s => s.Id == data.Id);

            if(existingData == null) return existingData;

            existingData.Name = data.Name;
            existingData.Address = data.Address;
            existingData.DateOfBirth = data.DateOfBirth;
            existingData.AccountId = data.AccountId;

            _context.Update(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }
    }
}
