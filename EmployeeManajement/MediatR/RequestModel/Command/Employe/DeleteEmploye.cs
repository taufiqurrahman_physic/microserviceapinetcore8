﻿using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteEmploye: IRequest<EmployeeViewModel>
    {
        public string Id { get; set; }

        public DeleteEmploye(string id)
        {
            Id = id;
        }
    }
}
