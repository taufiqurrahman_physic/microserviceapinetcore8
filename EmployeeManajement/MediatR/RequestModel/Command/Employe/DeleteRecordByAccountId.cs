﻿using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteRecordByAccountId : IRequest<bool>
    {
        public string AccountId { get; set; }

        public DeleteRecordByAccountId(string accountId)
        {
            AccountId = accountId;
        }
    }
}
