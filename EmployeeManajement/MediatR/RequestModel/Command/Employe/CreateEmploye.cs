﻿using EmployeeManajement.Entity;
using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.RequestModel.Command.Employe
{
    public class CreateEmploye : Employee, IRequest<EmployeeViewModel>
    {
        public CreateEmploye(string name, DateTime dateOfBirth, string address, string accountId)
        {
            Name = name;
            DateOfBirth = dateOfBirth;
            Address = address;
            AccountId = accountId;
        }
    }
}
