﻿using EmployeeManajement.Entity;
using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.RequestModel.Command.Employe
{
    public class UpdateEmploye : Employee, IRequest<EmployeeViewModel>
    {
        public UpdateEmploye(string id, string name, DateTime dateOfBirth, string address, string accountId)
        {
            Id = id;
            Name = name;
            DateOfBirth = dateOfBirth;
            Address = address;
            AccountId = accountId;
        }
    }
}
