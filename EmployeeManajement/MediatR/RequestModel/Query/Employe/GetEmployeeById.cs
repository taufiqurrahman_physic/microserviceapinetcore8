﻿using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.RequestModel.Query.Employe
{
    public class GetEmployeeById : IRequest<EmployeeViewModel>
    {
        public string Id { get; set; }

        public GetEmployeeById(string id)
        {
            Id = id;
        }
    }
}
