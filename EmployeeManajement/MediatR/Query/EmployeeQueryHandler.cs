﻿using AutoMapper;
using EmployeeManajement.InternalApiServices.Interface;
using EmployeeManajement.MediatR.RequestModel.Query.Employe;
using EmployeeManajement.Repository.Interface.ReadRepository;
using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.Query
{
    public class EmployeeQueryHandler : IRequestHandler<GetAllEmployees, IEnumerable<EmployeeViewModel>>,
                                        IRequestHandler<GetEmployeeById, EmployeeViewModel>
    {
        private readonly IEmployeQuery _repository;
        private readonly IMapper _mapper;
        private readonly IAccountApiService _apiService;

        public EmployeeQueryHandler(IEmployeQuery repository,
                                    IMapper mapper,
                                    IAccountApiService apiService)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
        }

        public async Task<IEnumerable<EmployeeViewModel>> Handle(GetAllEmployees request, CancellationToken cancellationToken)
        {
            var datas = await _repository.GetEmployees();

            if (datas == null || !datas.Any())
            {
                return null;
            }

            var result = datas.Select(e=>_mapper.Map<EmployeeViewModel>(e));

            var accounts = await _apiService.GetAccounts();

            if(accounts == null || !accounts.Any())
            {
                return result;
            }

            result = result.Select(s =>
            {
                var item = s;
                item.Account = accounts.FirstOrDefault(accounts => accounts.Id == item.AccountId);

                return item;
            });

            return result;
        }

        public async Task<EmployeeViewModel> Handle(GetEmployeeById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetEmploye(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<EmployeeViewModel>(data);
            result.Account =await _apiService.GetAccountFromId(request.Id);

            return result;
        }
    }
}
