﻿using AutoMapper;
using EmployeeManajement.Entity;
using EmployeeManajement.InternalApiServices.Interface;
using EmployeeManajement.MediatR.RequestModel.Command.Employe;
using EmployeeManajement.Repository.Interface.CommandRepository;
using EmployeeManajement.Repository.Interface.ReadRepository;
using EmployeeManajement.ViewModel;
using MediatR;

namespace EmployeeManajement.MediatR.Query
{
    public class EmployeeCommandHandler : IRequestHandler<CreateEmploye, EmployeeViewModel>,
                                        IRequestHandler<UpdateEmploye, EmployeeViewModel>,
                                        IRequestHandler<DeleteEmploye, EmployeeViewModel>,
                                        IRequestHandler<DeleteRecordByAccountId, bool>
    {
        private readonly IEmployeCommand _repository;
        private readonly IEmployeQuery _repositoryQuery;
        private readonly IMapper _mapper;
        private readonly IAccountApiService _apiService;
        private readonly IJobHistoryApiService _apiJobHistoryService;
        private readonly ISkillEmployeeApiService _apiSkillEmployeeService;

        public EmployeeCommandHandler(IEmployeCommand repository,
                                    IMapper mapper,
                                    IAccountApiService apiService,
                                    IJobHistoryApiService apiJobHistoryService,
                                    ISkillEmployeeApiService apiSkillEmployeeService,
                                    IEmployeQuery repositoryQuery)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
            _apiJobHistoryService = apiJobHistoryService;
            _apiSkillEmployeeService = apiSkillEmployeeService;
            _repositoryQuery = repositoryQuery;
        }

        public async Task<EmployeeViewModel> Handle(CreateEmploye request, CancellationToken cancellationToken)
        {
            var isAccountIdIsExist = await _repositoryQuery.IsAccountIdExist(request.AccountId);

            if (isAccountIdIsExist)
                throw new Exception("Account already used.");

            var account = await _apiService.GetAccountFromId(request.AccountId);

            if (account == null)
                throw new Exception("Account not found.");


            var data = await _repository.Create((Employee)request);

            if (data == null) throw new Exception("Failed create this data");

            var result = _mapper.Map<EmployeeViewModel>(data);
            result.Account = account;

            return result;
        }

        public async Task<EmployeeViewModel> Handle(UpdateEmploye request, CancellationToken cancellationToken)
        {
            var isAccountIdIsExist = await _repositoryQuery.IsAccountIdExist(request.AccountId, request.Id);

            if (isAccountIdIsExist)
                throw new Exception("Account already used.");

            var account = await _apiService.GetAccountFromId(request.AccountId);

            if (account == null)
                throw new Exception("Account not found.");

            var data = await _repository.Update((Employee)request);

            if (data == null) throw new Exception("Failed update this data");

            var result = _mapper.Map<EmployeeViewModel>(data);
            result.Account = account;

            return result;
        }

        public async Task<EmployeeViewModel> Handle(DeleteEmploye request, CancellationToken cancellationToken)
        {
            var data = await _repository.Delete(request.Id);

            if (data == null) throw new Exception("Failed delete this data");

            await _apiJobHistoryService.DeleteDatasByEmplyeeId(request.Id);

            await _apiSkillEmployeeService.DeleteDatasByEmplyeeId(request.Id);

            var result = _mapper.Map<EmployeeViewModel>(data);

            return result;
        }

        public async Task<bool> Handle(DeleteRecordByAccountId request, CancellationToken cancellationToken)
        {
            var employeeIds = await _repository.DeleteByAccountId(request.AccountId);

            if (employeeIds == null || !employeeIds.Any()) throw new Exception("Failed delete this data");

            foreach(var employeeId in employeeIds)
            {
                await _apiJobHistoryService.DeleteDatasByEmplyeeId(employeeId);

                await _apiSkillEmployeeService.DeleteDatasByEmplyeeId(employeeId);
            }

            return true;
        }
    }
}
