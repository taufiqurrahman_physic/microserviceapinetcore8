﻿using AccountManajement.Repository.Interface.CommandRepository;
using AccountManajement.Repository.Interface.ReadRepository;
using AccountManajement.Repository.Service.CommandRepository;
using AccountManajement.Repository.Service.ReadRepository;
using EmployeeManajement.InternalApiServices.Interface;
using EmployeeManajement.InternalApiServices.Service;

namespace AccountManajement.Extension
{
    public static class ServiceResolver
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IAccountCommand, AccountCommand>();
            services.AddScoped<IAccountQuery, AccountQuery>();
            services.AddScoped<IEmployeeApiService, EmployeeApiService>();
        }
    }
}
