﻿using EmployeeManajement.InternalApiServices.Interface;

namespace EmployeeManajement.InternalApiServices.Service
{
    public class EmployeeApiService : IEmployeeApiService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string _apiurl { get; set; }
        public EmployeeApiService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            _apiurl = @"https://localhost:5001/fiqapi/employee";
        }

        public async Task<bool> DeleteDatasByAccountId(string accountId)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.DeleteAsync(Path.Combine(_apiurl, "deleteByAccountId?accountId=" + accountId));

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    throw new Exception($"Failed to send DELETE Employee table. Status code: {response.StatusCode}");
                }
            }
        }
    }
}
