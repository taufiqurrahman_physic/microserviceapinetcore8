﻿using EmployeeManajement.ViewModel;

namespace EmployeeManajement.InternalApiServices.Interface
{
    public interface IEmployeeApiService
    {
        Task<bool> DeleteDatasByAccountId(string accountId);
    }
}
