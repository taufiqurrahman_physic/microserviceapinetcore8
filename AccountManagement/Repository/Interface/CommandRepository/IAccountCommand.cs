﻿using AccountManagement.Entity;

namespace AccountManajement.Repository.Interface.CommandRepository
{
    public interface IAccountCommand
    {
        Task<Account> Create(Account data);
        Task<Account> Update(Account data);
        Task<Account> Delete(string id);
    }
}
