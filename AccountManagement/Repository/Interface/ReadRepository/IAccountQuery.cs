﻿
using AccountManagement.Entity;

namespace AccountManajement.Repository.Interface.ReadRepository
{
    public interface IAccountQuery
    {
        Task<IEnumerable<Account>> GetAccounts();
        Task<Account> GetEmploye(string id);
        Task<Account> Authentication(string username,string password);
    }
}
