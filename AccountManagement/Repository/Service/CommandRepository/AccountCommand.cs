﻿using AccountManagement.Entity;
using AccountManajement.Context;
using AccountManajement.Repository.Interface.CommandRepository;
using Microsoft.EntityFrameworkCore;

namespace AccountManajement.Repository.Service.CommandRepository
{
    public class AccountCommand : IAccountCommand
    {
        private readonly AppDbContext _context;

        public AccountCommand(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Account> Create(Account data)
        {
            if (data == null)
                return data;

            data.Id = Guid.NewGuid().ToString();

            await _context.AddAsync(data);
            await _context.SaveChangesAsync();

            return data;
        }

        public async Task<Account> Delete(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) return null;

            var existingData = await _context.Accounts.FirstOrDefaultAsync(s=>s.Id == id);

            if(existingData == null) return existingData;

            _context.Remove(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }

        public async Task<Account> Update(Account data)
        {
            if (data == null) return data;

            var existingData = await _context.Accounts.FirstOrDefaultAsync(s => s.Id == data.Id);

            if(existingData == null) return existingData;

            existingData.Username = data.Username;
            existingData.Password = data.Password;
            existingData.Role = data.Role;

            _context.Update(existingData);
            await _context.SaveChangesAsync();

            return existingData;
        }
    }
}
