﻿using AccountManagement.Entity;
using AutoMapper;
using AccountManajement.ViewModel;
using Models;

namespace AccountManajement.AutoMapper
{
    public class AccountProfiles : Profile
    {
        public AccountProfiles() 
        {
            CreateMap<AccountViewModel, Account>()
                .ReverseMap();

            CreateMap<AuthenticationResponse, Account>()
                .ReverseMap()
                 .ForMember(dest => dest.Username, opt => opt.MapFrom(s => s.Username));
        }
    }
}
