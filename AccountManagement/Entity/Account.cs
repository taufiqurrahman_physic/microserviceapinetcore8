﻿using System.ComponentModel.DataAnnotations;

namespace AccountManagement.Entity
{
    public class Account
    {
        [Key]
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
