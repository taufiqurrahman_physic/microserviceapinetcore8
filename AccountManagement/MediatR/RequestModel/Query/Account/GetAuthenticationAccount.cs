﻿using AccountManagement.Entity;
using AccountManajement.ViewModel;
using MediatR;
using Models;

namespace AccountManagement.MediatR.RequestModel.Query.Employe
{
    public class GetAuthenticationAccount : Account, IRequest<AuthenticationResponse>
    {
        public GetAuthenticationAccount(string username, string password)
        {
            this.Username = username;
            this.Password = password;
        }
    }
}
