﻿using AccountManajement.ViewModel;
using MediatR;

namespace AccountManajement.MediatR.RequestModel.Command.Employe
{
    public class DeleteAccount: IRequest<AccountViewModel>
    {
        public string Id { get; set; }

        public DeleteAccount(string id)
        {
            Id = id;
        }
    }
}
