﻿using AccountManagement.Entity;
using AccountManajement.ViewModel;
using MediatR;

namespace AccountManajement.MediatR.RequestModel.Command.Employe
{
    public class CreateAccount : Account, IRequest<AccountViewModel>
    {
        public CreateAccount(string username, string password, string role)
        {
            this.Username = username;
            this.Password = password;
            this.Role = role;
        }
    }
}
