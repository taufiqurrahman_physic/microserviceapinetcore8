﻿using AccountManagement.Entity;
using AccountManajement.ViewModel;
using MediatR;

namespace AccountManajement.MediatR.RequestModel.Command.Employe
{
    public class UpdateAccount : Account, IRequest<AccountViewModel>
    {
        public UpdateAccount(string id, string username, string password, string role)
        {
            Id = id;
            Username = username;
            Password = password;
            Role = role;
        }
    }
}
