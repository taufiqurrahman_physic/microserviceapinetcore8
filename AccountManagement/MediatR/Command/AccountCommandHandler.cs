﻿using AccountManagement.Entity;
using AutoMapper;
using AccountManajement.MediatR.RequestModel.Command.Employe;
using AccountManajement.Repository.Interface.CommandRepository;
using AccountManajement.ViewModel;
using MediatR;
using EmployeeManajement.InternalApiServices.Interface;

namespace AccountManajement.MediatR.Query
{
    public class AccountCommandHandler : IRequestHandler<CreateAccount, AccountViewModel>,
                                        IRequestHandler<UpdateAccount, AccountViewModel>,
                                        IRequestHandler<DeleteAccount, AccountViewModel>
    {
        private readonly IAccountCommand _repository;
        private readonly IMapper _mapper;
        private readonly IEmployeeApiService _apiService;

        public AccountCommandHandler(IAccountCommand repository,
                                    IMapper mapper,
                                    IEmployeeApiService apiService)
        {
            _repository = repository;
            _mapper = mapper;
            _apiService = apiService;
        }

        public async Task<AccountViewModel> Handle(CreateAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Create((Account)request);

            if (data == null) return null;

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AccountViewModel> Handle(UpdateAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Update((Account)request);

            if (data == null) return null;

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AccountViewModel> Handle(DeleteAccount request, CancellationToken cancellationToken)
        {
            var data = await _repository.Delete(request.Id);

            if (data == null) return null;

            await _apiService.DeleteDatasByAccountId(request.Id);

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }
    }
}
