﻿using AccountManagement.MediatR.RequestModel.Query.Employe;
using AccountManajement.MediatR.RequestModel.Query.Employe;
using AccountManajement.Repository.Interface.ReadRepository;
using AccountManajement.ViewModel;
using AutoMapper;
using JwtAuthenticationManager;
using MediatR;
using Microsoft.IdentityModel.Tokens;
using Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace AccountManajement.MediatR.Query
{
    public class AccountQueryHandler : IRequestHandler<GetAllAccounts, IEnumerable<AccountViewModel>>,
                                       IRequestHandler<GetAccountById, AccountViewModel>,
                                       IRequestHandler<GetAuthenticationAccount, AuthenticationResponse>
    {
        private readonly IAccountQuery _repository;
        private readonly IMapper _mapper;

        public AccountQueryHandler(IAccountQuery repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AccountViewModel>> Handle(GetAllAccounts request, CancellationToken cancellationToken)
        {
            var datas = await _repository.GetAccounts();

            if (datas == null)
            {
                return null;
            }

            return datas.Select(e=>_mapper.Map<AccountViewModel>(e));
        }

        public async Task<AccountViewModel> Handle(GetAccountById request, CancellationToken cancellationToken)
        {
            var data = await _repository.GetEmploye(request.Id);

            if(data == null)
            {
                return null;
            }

            var result = _mapper.Map<AccountViewModel>(data);

            return result;
        }

        public async Task<AuthenticationResponse> Handle(GetAuthenticationAccount request, CancellationToken cancellationToken)
        {
            var user = await _repository.Authentication(request.Username, request.Password);

            if (user == null)
            {
                return null;
            }

            var result = _mapper.Map<AuthenticationResponse>(user);

            var tokenExpiryTimeStamp = DateTime.Now.AddMinutes(JwtTokenHandler.JWT_TOKEN_VALIDATION);
            var tokenKey = Encoding.ASCII.GetBytes(JwtTokenHandler.GetHexadecimalStringToken());
            var claimIdentity = new ClaimsIdentity(new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Name, user.Username),
                new Claim(ClaimTypes.Role, user.Role)
            });

            var signingCredentials = new SigningCredentials(
                new SymmetricSecurityKey(tokenKey),
                SecurityAlgorithms.HmacSha256Signature);

            var securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimIdentity,
                Expires = tokenExpiryTimeStamp,
                SigningCredentials = signingCredentials
            };

            var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
            var securityToken = jwtSecurityTokenHandler.CreateToken(securityTokenDescriptor);
            var token = jwtSecurityTokenHandler.WriteToken(securityToken);

            result.JwtToken = token;
            result.ExpiresIn = (int)tokenExpiryTimeStamp.Subtract(DateTime.Now).TotalSeconds;

            return result;
        }

    }
}
